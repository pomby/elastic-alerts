const { Client } = require('@elastic/elasticsearch');
let eClient = null;
let opt = {};

const STATUS = Object.freeze({
  success: 1,
  fail: 2,
  noResponse: 4,
  error: 8,
  slow: 16,
  testFail: 32,
  badConfig: 64,
});

function replaceAll(str,mapObj){
  let out = str;

  for(const rep in mapObj){
    if(mapObj.hasOwnProperty(rep)){
      out = out.replaceAll(`{${rep}}`, mapObj[rep]);
    }
  }

  return out;
}

const evalConfig = async (config, monitor, runLog, error, errorDetails) => {
  let type = 0;
  let isType = [];

  if(config.type){
    if(Array.isArray(config.type)){
      config.type.forEach(e => {
        if(STATUS[e]){
          type = type | STATUS[e];
          isType.push(e);
        }
      })
    }else if(STATUS[e]){
      type = STATUS[e];
    }
  }

  if(type & runLog.status){

    let message = '';
    // matches at least 1 status defined
    if(config.template){
      message = replaceAll(config.template, {
        title: monitor.title,
        runTime: runLog.runTime,
        runTimeISO: runLog.runTime ? runLog.runTime.toISOString() : '-',
        timing: runLog.timing,
        statusCode: errorDetails ? errorDetails.status : '-',
        type: monitor.type,
        category: monitor.category,
        uri: monitor.uri,
      });
    }else{
      // default message
      message = `Elastic Alert triggered by: ${monitor.title}`;
    }

    const idx = typeof opt.index === 'function ' ? opt.index(runLog.runTime) : opt.index;
    const tsField = opt.timestampField || '@timestamp';

    const body = {
      message: message,
      statusTypes: isType,
      monitor: monitor.title,
      timing: runLog.timing,
      monitorType: monitor.type,
      category: monitor.category,
      uri: monitor.uri,
    };

    if(errorDetails){
      body.statusCode = errorDetails.status;
      body.errorDetails = error.details;
    }

    body[tsField] = runLog.runTime;

    eClient.index({
      index: idx,
      body: body,
    }).catch((error) => {
      console.error('💥 Elastic Alerts error inserting data', error);
    })

  }
}

module.exports = (options) => {

  eClient = new Client({
    node: options.node,
    proxy: options.proxy,
    auth: options.auth,
    cloud: options.cloud,
    ssl: options.ssl ? options.ssl : { rejectUnauthorized: options.rejectUnauthorized },
  });

  opt = options;
  const idx = typeof options.index === 'function ' ? options.index(new Date()) : options.index;

  eClient.indices.get({index: idx, ignoreUnavailable: true}).then((data) => {
    console.info('💡 Connection to elasticsearch established');
  }).catch((error) => {
    console.error('💥 Elasticsearch cluster could not be reached!', error);
  });

  return {
    name: 'Elastic Alerts',
    onRunComplete: async (config, monitor, runLog, error, errorDetails) => {
      if(config.elasticAlerts){
        if(Array.isArray(config.elasticAlerts)){
          for(const subConf of config.elasticAlerts) {
            await evalConfig(subConf, monitor, runLog, error, errorDetails);
          }
        }else{
          await evalConfig(config.elasticAlerts, monitor, runLog, error, errorDetails);
        }
      }
    }
  }
};

/**
 * String.prototype.replaceAll() polyfill
 * https://gomakethings.com/how-to-replace-a-section-of-a-string-with-another-one-with-vanilla-js/
 * @author Chris Ferdinandi
 * @license MIT
 */
 if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function(str, newStr){

		// If a regex pattern
		if (Object.prototype.toString.call(str).toLowerCase() === '[object regexp]') {
			return this.replace(str, newStr);
		}

		// If a string
		return this.replace(new RegExp(str, 'g'), newStr);

	};
}
