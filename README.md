# @pomby/consolealerts

A library for use with pomby to add alerts to the console

![sample kibana](Screenshot.png)

## Configuring

```javascript
const elasticAlerts = require('@pomby/elasticalerts');

const eaPlugin = elasticAlerts({
  index: 'pombyalerts',
  node: 'http://localhost:9200',
});

pomby({
  //...
  plugins: [eaPlugin],
});

```

### Options
* __index__ - can be a string or a function, if a function the runtime of the monitor will be passed
* __node__ - node or array of nodes to connect to
* __timestampField__ (optional) - defaults to '@timestamp'
* _more_ - You can send in most options available to the [elastic search client](https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/client-connecting.html) connect


## Sample Config

In your config you can add something like the following to your config:

```javascript
{
  "elasticAlerts": [
    {
      "type": ["fail"],
      "template": "[{runTimeISO}] There was an error: {statusCode} with monitor: {title} in category {category}"
    }
  ]
}
```

The type is the only required portion, output will default to info and template will default to `"Console Alert Triggered for: {title}"`

## Template Variables

* __title__ - Title of the monitor
* __runTime__ - Date of the run
* __runTimeISO__ - Date fo the run but formatted in ISO format
* __timing__ - Time in ms the run took
* __statusCode__ - Status code of the run (only on error)
* __type__ - Monitor Type used 
* __category__ - Monitor Category

## Types:

* success
* fail
* noResponse
* error
* slow
* testFail
* badConfig
